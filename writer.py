import os
import cv2
import time
import h5py
from video import gen_frame, particles_2_frame
import numpy as np


def write_to_file(hf, new_pos, n_particles):
    if 'data' in list(hf.keys()):
        new_size = (hf["data"].shape[0] + 1,) + hf["data"].shape[1:]
        hf["data"].resize(new_size,)
        hf["data"][hf["data"].shape[0]-1] = new_pos
        hf["data"].flush()
        print('wrote..')
    else:
        dset = hf.create_dataset("data", shape=(1, n_particles, 2), data=[new_pos],
                                 dtype='f4', maxshape=(None, n_particles, 2),)
        hf.swmr_mode = True
        dset.flush()


def listener(messageQ, dataQ, n_particles, path='./swmr.h5', write_video=True, write_h5=False):
    '''listens for messages on the q, writes to file. '''
    if os.path.exists(path):
        os.remove(path)

    nbins = (int(1920), int(1080))
    # nbins = (int(3840*0.6), int(2160*0.6))

    if write_video:
        video_writer = cv2.VideoWriter('out.avi', cv2.VideoWriter_fourcc(*'mp4v'), 15, nbins)
    while 1:
        if messageQ.qsize() > 0:
            m = messageQ.get()
            if m == 'kill':
                if write_video:
                    video_writer.release()
                break
        while dataQ.queue.qsize() > 0:
            new_pos = dataQ.get()
            if write_h5:
                with h5py.File(path, 'a', libver='latest') as hf:
                    write_to_file(hf, new_pos, n_particles)
            if write_video:
                print('Wrote frame')
                start = time.perf_counter()
                cf = gen_frame(new_pos[:, 0], new_pos[:, 1], nbins)
                stop = time.perf_counter()
                print('hist', stop-start)
                start = time.perf_counter()
                print('sent')
                video_writer.write(cf)
                stop = time.perf_counter()
                print('cvwrite', stop-start)

        time.sleep(0.0001)


async def async_listener(dataQ, n_particles, websocket=None):
    '''listens for messages on the q, writes to file. '''
    nbins = (int(200), int(200))

    while dataQ.queue.qsize() > 0:
        start = time.perf_counter()
        new_pos = dataQ.get()
        start = time.perf_counter()
        bw_frame = particles_2_frame(new_pos[:, 0], new_pos[:, 1], nbins)
        await websocket.send_bytes(bw_frame.astype(np.uint8).ravel().tobytes())
        stop = time.perf_counter()
        print('Socket sent:', stop-start)

        # time.sleep(0.0001)


async def async_listener2(new_pos, n_particles, extent, websocket=None, nbins=(200, 200)):
    '''listens for messages on the q, writes to file. '''

    start = time.perf_counter()
    bw_frame = particles_2_frame(new_pos[:, 0], new_pos[:, 1], nbins, xedges=extent[0], yedges=extent[1])
    await websocket.send_bytes(bw_frame.astype(np.uint8).ravel().tobytes())
    stop = time.perf_counter()
    print('Socket sent:', stop-start)


async def async_listener_packages(new_pos_arr, n_particles, extent, websocket=None, nbins=(200, 200)):
    '''listens for messages on the q, writes to file. '''

    start = time.perf_counter()
    frame_buffer = np.zeros([len(new_pos_arr), nbins[0], nbins[1]])
    for i, new_pos in enumerate(new_pos_arr):
        bw_frame = particles_2_frame(new_pos[:, 0], new_pos[:, 1], nbins, xedges=extent[0], yedges=extent[1])
        frame_buffer[i] = bw_frame
    await websocket.send_bytes(frame_buffer.astype(np.uint8).ravel().tobytes())
    stop = time.perf_counter()
    print('Socket sent:', stop-start)

    # time.sleep(0.0001)
