# old.py

import numpy as np
import torch
import matplotlib.pyplot as plt

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def plotgrid(nodes, triangles, model, particles=None):
    # midpoints = nodes[triangles.simplices].mean(axis=1)
    fig, ax = plt.subplots()
    plt.triplot(nodes[:, 0], nodes[:, 1], triangles.simplices)
    plt.plot(nodes[:, 0], nodes[:, 1], 'o')
    # plt.plot(midpoints[:, 0], midpoints[:, 1], '*')
    # field_nodes = np.asarray([model['u'][0, :], model['v'][0, :]]).T
    # q = plt.quiver(nodes[:, 0], nodes[:, 1], field_nodes[:, 0], field_nodes[:, 1])
    # if particles is not None:
    #     plt.plot(particles[:, 0], particles[:, 1], '*', color='blue')
    # plt.show()


def constructNneighbors(triangles):
    neighbors = np.asarray(triangles.neighbors)
    kNeig = []
    negAset = set([-1, ])
    kNeig.append(np.array([np.array(list(set(s) - negAset)) for s in neighbors]))
    for k in range(2, 8):
        print(k)
        if k > 2:
            new_neigh_of_neig = [set(np.concatenate(neighbors[prev])) - negAset - set([i,]) - set(np.concatenate([prevvi[i] for prevvi in kNeig])  ) for i, prev in enumerate(kNeig[k-2])]
        else:
            new_neigh_of_neig = [set(np.concatenate(neighbors[prev])) - negAset - set([i,])  for i, prev in enumerate(kNeig[k-2])]
        kNeig.append(np.array([np.array(list(s)) for s in new_neigh_of_neig]))

    limit = 9
    extendedNeighbours = [np.concatenate((kNeig[0][i],    kNeig[1][i],  kNeig[2][i],  kNeig[4][i],  kNeig[5][i], kNeig[6][i],))[:limit] for i in range(len(neighbors))]
    return extendedNeighbours


def calc_bary_cpu(tri, p, tri_idx):
    g = np.transpose(p - tri.transform[tri_idx, 2])
    b = tri.transform[tri_idx, :2].dot(g)
    ret = np.c_[np.transpose(b), 1 - b.sum(axis=0)]
    ret = ret.ravel()
    return ret


def calc_bary(tri_transform_t, p, tri_idx):
    tri_idx_t = torch.tensor(tri_idx, device=device)
    p_t = torch.tensor(p, device=device).float()  # .half()
    g = (p_t - tri_transform_t[tri_idx_t, 2]).t()  # OK
    b = torch.matmul(tri_transform_t[tri_idx_t, :2], g)  # ok
    ret = b.t().cpu().detach().numpy()
    ret2 = (1-b.sum(axis=0)).cpu().detach().numpy()
    ret = ret.ravel()
    ret = np.concatenate([ret, ret2])
    return ret


def calc_bary_par(tri_transform_t, p_t, tri_idx):
    tri_idx_t = torch.tensor(tri_idx, device=device).int()
    g = (p_t - tri_transform_t[tri_idx_t, 2])  # OK
    g = g.view(g.shape+ (1,))
    b = torch.matmul(tri_transform_t[tri_idx_t,:2], g)  # ok
    c = 1-b.sum(axis=1)
    c.resize_((1,)+c.shape)
    ret = torch.cat((b.transpose(0,1), c), 0)
    ret = ret.view(list(ret.shape)[:-1])
    ret = ret.transpose(0,1)
    return ret


# calc our min dist?
# w/0 python3 main.py --debug False --n_particles 2000000 --extent 1000000.  100000  90.41s user 3.48s system 105% cpu 1:28.92 total
# w/ python3 main.py --debug False --n_particles 2000000 --extent 1000000.  100000  89.87s user 3.71s system 103% cpu 1:30.33 total
# if (sum_all<1000) and (sum_all>0):
#     dists = torch.cdist(particles[wrong_weights_b],trimids_t)
#     mins = torch.argmin(dists, dim=1)
#     tw_t.data[wrong_weights_b] = mins#[correct_weights_b]
#     miniWeights = calc_Barycentric(nodes_t[triangles_simplices_t[mins]], particles[wrong_weights_b])
#     correct_weights_b = check_inside(miniWeights)==True
#     wrong_weights_b[wrong_weights_b] = correct_weights_b
#     weights.data[wrong_weights_b] = miniWeights[correct_weights_b]
#
#     # wrong_weights_b = check_inside(weights)==False
#     wrong_weights_b = wrong_weights_b & active_t
#
#     if debug:
#         sum_all = torch.sum(wrong_weights_b).int()
#         print(sum_all.cpu().numpy(), end=' >> ')
#
#     for l in range(14):
#         t_idxs_t = twentyNeigbors_t[tw_t[wrong_weights_b]][:,l]
#         miniWeights = calc_Barycentric(nodes_t[triangles_simplices_t[t_idxs_t]], particles[wrong_weights_b])
#         correct_weights_b = check_inside(miniWeights)==True
#         # sum_correct = torch.sum(correct_weights_b).int()
#         # print('caught:',sum_correct.cpu().numpy())
#         wrong_weights_b[wrong_weights_b] = correct_weights_b
#         weights.data[wrong_weights_b] = miniWeights[correct_weights_b]
#         tw_t.data[wrong_weights_b] = t_idxs_t[correct_weights_b]
#         wrong_weights_b = check_inside(weights)==False
#         wrong_weights_b = wrong_weights_b & active_t
#
#     if debug:
#         sum_all = torch.sum(wrong_weights_b).int()
#         print(sum_all.cpu().numpy())
