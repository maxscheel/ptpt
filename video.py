import cv2
import h5py
import click
import numpy as np
import multiprocessing as mp
import matplotlib.pyplot as plt
from fast_histogram import histogram2d


def get_extent():
    xedges, yedges = [1567560.0, 1664054.0], [5424407.0, 5528537.0]

    # x0 = xedges[0] + (xedges[1]-xedges[0])*1.5/10
    # x1 = xedges[0] + (xedges[1]-xedges[0])*6.5/10

    # y0 = yedges[0] + (yedges[1]-yedges[0])*0./8
    # y1 = yedges[0] + (yedges[1]-yedges[0])*6./8

    x0 = xedges[0] + (xedges[1]-xedges[0])*4.5/10
    x1 = xedges[0] + (xedges[1]-xedges[0])*6.5/10
    y0 = yedges[0] + (yedges[1]-yedges[0])*0/8
    y1 = yedges[0] + (yedges[1]-yedges[0])*1/8
    xedges = [x0, x1]
    yedges = [y0, y1]
    return xedges, yedges


def particles_2_frame(x, y, nbins, xedges=None, yedges=None):
    if xedges is None:
        xedges, yedges = get_extent()
    img = histogram2d(x, y, bins=nbins, range=[xedges, yedges]).T
    # img = np.log2(img+1)
    img = img/(np.max(img))*255.
    img = np.flipud(img)
    return img


def gen_frame(x, y, nbins, cmap='viridis'):
    midpoint, region = [x[0], y[0]], 5000
    # midpoint, region = [1618392., 5445403.829], 50000
    img = particles_2_frame(x, y, nbins, [midpoint[0]-region, midpoint[0]+region],
                                         [midpoint[1]-region, midpoint[1]+region])
    c_map = plt.cm.get_cmap(cmap)
    img = (c_map(img)[:, :, :3]*255.).astype('uint8')
    cv2.cvtColor(img, cv2.COLOR_RGB2BGR, img)
    return img


def get_nbins():
    return (int(1920/2), int(1080/2))


def gen_frame_mp(i):
    # global hf
    # print(hf)
    particle_limit = None
    nbins = get_nbins()
    with h5py.File('./swmr.h5', 'r', libver='latest', swmr=True) as hf:
        cf = gen_frame(hf['data'][i, :particle_limit, 0], hf['data'][i, :particle_limit, 1], nbins=nbins)
    return cf


@click.command()
@click.option('--path', default='./swmr.h5', type=str, help='Number of greetings.')
@click.option('--cmap', default='cividis', type=str, help='Colormap')
@click.option('--kkkk', default=False, type=bool, help='4k')
@click.option('--out', default='test1.avi', type=str, help='output')
def main(path, cmap, kkkk, out):
    # global hf
    with h5py.File(path, 'r', libver='latest', swmr=True) as hf:
        timesteps = hf['data'].shape[0]

        # xedges, yedges = get_extent()
        nbins = get_nbins()
        video = cv2.VideoWriter(out, cv2.VideoWriter_fourcc(*'mp4v'), 24, nbins)
        nProcesses = 6
        p = mp.Pool(nProcesses)

        for i in range(0, timesteps, nProcesses):
            # print(i)
            idx = i + np.arange(nProcesses)
            print(idx)
            cf_list = p.map(gen_frame_mp, idx.tolist())
            for cf in cf_list:
                video.write(cf)

        # for i in range(timesteps):
        #     print(i)
        #     cf = gen_frame(hf['data'][i,:particle_limit,0],hf['data'][i,:particle_limit,1], nbins, cmap)
        #     video.write(cf)
        video.release()
    # cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
