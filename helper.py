'''
PyTorch Particle Tracker - PTPT
Max Scheel 2020
max@max.ac.nz
'''

import torch
import pickle
import numpy as np
from time import perf_counter
from scipy.spatial import cKDTree


def get_tree(nodes, triangles, tree_file='tree.pkl'):
    '''
    Generate tree if neccesary, otherwise load from pickle file.
    '''
    try:
        with open(tree_file, 'rb') as pkl_file:
            tree = pickle.load(pkl_file)
    except:
        with open(tree_file, 'wb') as output:
            midpoints = np.mean(nodes[triangles.simplices], axis=1)
            tree = cKDTree(np.c_[midpoints[:, 0], midpoints[:, 1]], leafsize=100,  balanced_tree=True)
            pickle.dump(tree, output)
    return tree


def calc_Barycentric(triangle_coord_triplet, p):
    '''
    Calculate the barycentric coordinates of points `p` with respect to the triangle coordinated triplet \
    `triangle_coord_triplet`.

    Point p, Point a, Point b, Point c, float &u, float &v, float &w)
    Vector v0 = b - a, v1 = c - a, v2 = p - a;
    float d00 = Dot(v0, v0);
    float d01 = Dot(v0, v1);
    float d11 = Dot(v1, v1);
    float d20 = Dot(v2, v0);
    float d21 = Dot(v2, v1);
    float denom = d00 * d11 - d01 * d01;
    v = (d11 * d20 - d01 * d21) / denom;
    w = (d00 * d21 - d01 * d20) / denom;
    u = 1.0f - v - w;
    '''

    torch.cuda.nvtx.range_push("calc_Barycentric")
    out = torch.empty([triangle_coord_triplet.shape[0], 3], device=p.device, dtype=p.dtype)
    v1 = triangle_coord_triplet[:, 2] - triangle_coord_triplet[:, 0]
    v0 = triangle_coord_triplet[:, 1] - triangle_coord_triplet[:, 0]
    v2 = p - triangle_coord_triplet[:, 0]
    d11 = torch.sum(v1 * v1, axis=1)
    d20 = torch.sum(v2 * v0, axis=1)
    d01 = torch.sum(v0 * v1, axis=1)
    d00 = torch.sum(v0 * v0, axis=1)
    d21 = torch.sum(v2 * v1, axis=1)
    denom = d00 * d11 - d01 * d01
    out.data[:, 1] = (d11 * d20 - d01 * d21) / denom
    out.data[:, 2] = (d00 * d21 - d01 * d20) / denom
    out.data[:, 0] = 1.0 - out[:, 1] - out[:, 2]
    torch.cuda.nvtx.range_pop()

    return out


def gen_bary_cache_t(triangles_simplices_t, nodes_t):
    '''
    Generate cache of calculations for barycentric coordinates that are independent from the particle position
    '''
    num_triangles = list(triangles_simplices_t.shape)
    print(num_triangles)
    bary_cache_t = torch.zeros([num_triangles[0], 8], device=nodes_t.device, dtype=nodes_t.dtype)
    bary_cache_t[:, 0:2] = nodes_t[triangles_simplices_t][:, 1] - nodes_t[triangles_simplices_t][:, 0]  # v0_cached
    bary_cache_t[:, 2:4] = nodes_t[triangles_simplices_t][:, 2] - nodes_t[triangles_simplices_t][:, 0]  # v1_cached
    bary_cache_t[:, 4] = torch.sum(bary_cache_t[:, 0:2] * bary_cache_t[:, 0:2], axis=1)  # d00_cached
    bary_cache_t[:, 5] = torch.sum(bary_cache_t[:, 0:2] * bary_cache_t[:, 2:4], axis=1)  # d01_cached
    bary_cache_t[:, 6] = torch.sum(bary_cache_t[:, 2:4] * bary_cache_t[:, 2:4], axis=1)  # d11_cached
    bary_cache_t[:, 7] = 1./(bary_cache_t[:, 4] * bary_cache_t[:, 6] - bary_cache_t[:, 5] * bary_cache_t[:, 5])
    return bary_cache_t


def calc_Barycentric_cached(triangle_coord_triplet, p, bary_cached):
    '''
    Use `bary_cached` to reduce the number of kernel operations performed and do a lookup instead.
    # 0 v0 = bary_cached  0:2
    # 1 v1 = bary_cached  2:4
    # 2 d00 = bary_cached 4
    # 3 d01 = bary_cached 5
    # 4 d11 = bary_cached 6
    # 5 invdenom = bary_cached 7
    '''

    ret = torch.empty([triangle_coord_triplet.shape[0], 3], device=p.device, dtype=p.dtype)
    v2 = p - triangle_coord_triplet.data[:, 0]
    d20 = torch.sum(v2 * bary_cached[:, 0:2], axis=1)
    d21 = torch.sum(v2 * bary_cached[:, 2:4], axis=1)
    ret[:, 1] = (bary_cached[:, 6] * d20 - bary_cached[:, 5] * d21) * bary_cached[:, 7]
    ret[:, 2] = (bary_cached[:, 4] * d21 - bary_cached[:, 5] * d20) * bary_cached[:, 7]
    ret[:, 0] = 1.0 - ret[:, 1] - ret[:, 2]
    return ret


def check_inside(weights, eps=-1e-5):
    '''
    Check components of the weights if querypoint is inside the triangle (0 < w_i < 1 ).
    Be a bit more generous about what the term 'inside' means and slacken off to an epsilon.
    '''
    return ((weights[:, 0] > eps) & (weights[:, 1] > eps) & ((weights[:, 0] + weights[:, 1]) < 1.00001))


def get_field_from_GPU_buffered(model, i, field_buffer_t):
    '''
        Load the velocity field into a buffer tensor.
    '''
    buffersize = field_buffer_t.shape[0]
    half_buffersize = int(buffersize/2)
    dev = field_buffer_t.device
    # get_model_start = perf_counter()
    # print('buffer refill required...')
    if i == 0:
        u_s = model['u'][i:i+buffersize, :]
        v_s = model['v'][i:i+buffersize, :]
        field_buffer_t.data = torch.tensor([u_s, v_s], device=dev).transpose(0, 1).transpose(1, 2)
    elif (i % buffersize) == 0:
        u_s = model['u'][i+half_buffersize:i+buffersize, :]
        v_s = model['v'][i+half_buffersize:i+buffersize, :]
        field_buffer_t.data[half_buffersize:] = torch.tensor([u_s, v_s], device=dev).transpose(0, 1).transpose(1, 2)
    elif (i % buffersize) == half_buffersize:
        u_s = model['u'][i+half_buffersize:i+buffersize, :]
        v_s = model['v'][i+half_buffersize:i+buffersize, :]
        field_buffer_t.data[:half_buffersize] = torch.tensor([u_s, v_s], device=dev).transpose(0, 1).transpose(1, 2)
    # get_model_stop = perf_counter()
    # print('get model', get_model_stop-get_model_start)


def get_weights(triangle_nodes_coords_t, triangle_3neigbors_t, previous_triangle_t, tree, kmax, particles,
                bary_cache_t, active_t, log_writer=None):
    '''
        Locate position of `particles` within the grid cells as well as calculate the barycentric weights
        for each particles triangle. The initial guess `previous_triangle_t` is getting updated with each
        particle being found.
        1) Check if the particle is within the previous triangle
        2) If weights for the previous triangle index are negative, try a neighboring triangle, \
           the direction is thereby given by the index of the smallest (e.g. most negative) weight.
        3) Repeat until found (is_inside(weights_i) == True)
            a) Stop if done too many steps (too far away.)
            b) Stop if suggested neighbor_idx is -1 (Out of bounds)
        4) For remainder of unfound active particles: Fallback on K-nearest neighbor search.
        5) If still not found. Deactivate particle.
    '''

    if log_writer:
        calc_current_bary_start = perf_counter()

    if bary_cache_t:
        weights = calc_Barycentric_cached(triangle_nodes_coords_t[previous_triangle_t], particles,
                                          bary_cache_t[previous_triangle_t])
    else:
        weights = torch.zeros((particles.shape[0], 3), device=particles.device, dtype=particles.dtype)
        weights.data = calc_Barycentric(triangle_nodes_coords_t[previous_triangle_t], particles)

    torch.cuda.nvtx.range_push("check_inside, active and assign")
    wrong_weights_b = ~check_inside(weights) & active_t
    torch.cuda.nvtx.range_pop()

    torch.cuda.nvtx.range_push("nonzero")
    wrong_weights_idx = torch.nonzero(wrong_weights_b, as_tuple=False).view(-1)
    torch.cuda.nvtx.range_pop()

    if log_writer:
        torch.cuda.synchronize()
        calc_current_bary_fin = perf_counter()

    if log_writer:
        assigned_by_MUT = 0
        MUT_start = perf_counter()

    idxx = 0
    while idxx < 20:
        if wrong_weights_idx.shape[0] > 0:
            N_idx = (torch.argmin(weights.data[wrong_weights_idx], axis=1))
            # N_idx = (torch.argmin(torch.index_select(weights, 0, wrong_weights_idx), axis=1))
            t_idxs_t = triangle_3neigbors_t[previous_triangle_t[wrong_weights_idx], N_idx]
            # print(t_idxs_t)
            outside_grid_b = (t_idxs_t == -1)
            found_outside = wrong_weights_idx[outside_grid_b]
            if found_outside.shape[0] > 0:
                # print('>>>>>>>>>> found', found_outside.shape[0], 'outside grid')
                if log_writer:
                    assigned_by_MUT += found_outside.shape[0]
                wrong_weights_b.data[found_outside] = False
                inside_grid = ~outside_grid_b
                wrong_weights_idx = wrong_weights_idx[inside_grid]
                t_idxs_t = t_idxs_t[inside_grid]

            if bary_cache_t:
                miniWeights = calc_Barycentric_cached(triangle_nodes_coords_t[t_idxs_t], particles[wrong_weights_idx],
                                                      bary_cache_t[t_idxs_t])
            else:
                miniWeights = calc_Barycentric(triangle_nodes_coords_t[t_idxs_t], particles[wrong_weights_idx])
            weights.data[wrong_weights_idx] = miniWeights
            previous_triangle_t.data[wrong_weights_idx] = t_idxs_t
            correct_weights_b = check_inside(miniWeights).bool()
            goodie = wrong_weights_idx[correct_weights_b]
            if goodie.shape[0] > 0:
                # print('{} Found inside {}'.format(idxx, goodie.shape[0]))
                if log_writer:
                    assigned_by_MUT += goodie.shape[0]
                wrong_weights_idx = wrong_weights_idx[~correct_weights_b]
                wrong_weights_b.data[goodie] = False

            idxx = idxx + 1
        else:
            break

    if log_writer:
        MUT_fin = perf_counter()
        assigned_pct_MUT = assigned_by_MUT/weights.shape[0]
        time_MUT = MUT_fin-MUT_start
        idxx_MUT = idxx
        efficiency_MUT = assigned_by_MUT/time_MUT
        assigned_by_KNN = 0
        startKNN = perf_counter()

    if wrong_weights_idx.shape[0] > 0:
        parts = particles[wrong_weights_idx]
        n_jobs = 1 if parts.shape[0] < 1000 else -1
        if log_writer:
            time_knn_cpu_start = perf_counter()

        _, t_idxs = tree.query(parts.cpu().numpy(), k=kmax, n_jobs=n_jobs, eps=0.0)

        if log_writer:
            time_knn_cpu_stop = perf_counter()
            time_kkn_gpu_start = perf_counter()

        t_idxs_buffer_t = torch.tensor(t_idxs, device=wrong_weights_idx.device, dtype=torch.long)
        sum_allwrong = 1
        temp_weights = torch.zeros([parts.shape[0], 3], device=wrong_weights_idx.device, dtype=particles.dtype)
        temp_idxs_t = torch.zeros(parts.shape[0], device=wrong_weights_idx.device, dtype=torch.long)
        allWrong = torch.ones(parts.shape[0], device=wrong_weights_idx.device, dtype=torch.bool)
        k = 0
        while sum_allwrong > 0:
            k += 1
            if k > kmax:
                print('>>>>>>>>>> Exited and deactivated: {} particles'.format(sum_allwrong))
                break

            t_idxs_t = t_idxs_buffer_t[allWrong][:, k-1]
            temp_weights.data[allWrong] = calc_Barycentric(triangle_nodes_coords_t[t_idxs_t], parts[allWrong])
            temp_idxs_t.data[allWrong] = t_idxs_t
            allright = check_inside(temp_weights)
            allWrong.data[allright] = False
            sum_allwrong = torch.sum(allWrong)

        previous_triangle_t.data[wrong_weights_idx] = temp_idxs_t
        weights.data[wrong_weights_idx] = temp_weights
        wrong_weights_b.data[wrong_weights_idx] = allWrong
        # some leftover wrong ones might end up here if k>kmax and hasn't been found

        if log_writer:
            assigned_by_KNN += temp_weights.shape[0]
            time_kkn_gpu_stop = perf_counter()
            w_dev = '({})'.format(weights.device)
            log_writer('/Time', {'KNN/GPU'+w_dev: time_kkn_gpu_stop-time_kkn_gpu_start})
            log_writer('/Time', {'KNN/CPU'+w_dev: time_knn_cpu_stop-time_knn_cpu_start})
            log_writer('/Particles', {'Lost'+w_dev: sum_allwrong})

    if log_writer:
        stopKNN = perf_counter()
        assigned_pct_KNN = assigned_by_KNN/weights.shape[0]
        time_KNN = stopKNN-startKNN
        efficiency_KNN = assigned_by_KNN/time_KNN
        assigned_by_PTE = weights.shape[0] - assigned_by_KNN - assigned_by_MUT
        assigned_pct_PTE = assigned_by_PTE/weights.shape[0]
        time_PTE = calc_current_bary_fin-calc_current_bary_start
        efficiency_PTE = assigned_by_PTE/time_PTE
        w_dev = '({})'.format(weights.device)
        log_writer('/Time', {'PTE/total'+w_dev: time_PTE,
                             'MUT/total'+w_dev: time_MUT,
                             'KNN/total'+w_dev: time_KNN,
                             })
        log_writer('/Efficiency', {'PTE'+w_dev: efficiency_PTE,
                                   'MUT'+w_dev: efficiency_MUT,
                                   'KNN'+w_dev: efficiency_KNN,
                                   })
        log_writer('/Assigned_pct', {'PTE'+w_dev: assigned_pct_PTE,
                                     'MUT'+w_dev: assigned_pct_MUT,
                                     'KNN'+w_dev: assigned_pct_KNN
                                     })
        log_writer('/Index', {'MUT'+w_dev: idxx_MUT})

    return weights, wrong_weights_b, wrong_weights_idx


def get_weights_seq(triangle_nodes_coords_t, triangle_3neigbors_t, previous_triangle_t, tree, kmax, particles,
                    bary_cache_t, active_t, log_writer=None):
    '''
        Locate position of `particles` within the grid cells as well as calculate the barycentric weights
        for each particles triangle. The initial guess `previous_triangle_t` is getting updated with each
        particle being found.
        1) Check if the particle is within the previous triangle
        2) If weights for the previous triangle index are negative, try a neighboring triangle, \
           the direction is thereby given by the index of the smallest (e.g. most negative) weight.
        3) Repeat until found (is_inside(weights_i) == True)
            a) Stop if done too many steps (too far away.)
            b) Stop if suggested neighbor_idx is -1 (Out of bounds)
        4) For remainder of unfound active particles: Fallback on K-nearest neighbor search.
        5) If still not found. Deactivate particle.
    '''

    weights = torch.zeros([particles.shape[0], 3], device=particles.device, dtype=particles.dtype)
    wrong_weights_b = torch.ones(particles.shape[0], device=particles.device, dtype=torch.bool)

    for i in range(particles.shape[0]):
        if active_t[i]:
            pt_i = previous_triangle_t[i]
            part_pos_i = particles[i]
            w_i = calc_Barycentric(triangle_nodes_coords_t[pt_i][None, :], part_pos_i[None, :])
            not_found = True
            idxx = 0
            inside = check_inside(w_i)
            if (inside):
                not_found = False
            else:
                while (~inside & (idxx < 20)):
                    N_idx = (torch.argmin(w_i, axis=1))
                    t_idxs_t = triangle_3neigbors_t[pt_i, N_idx]
                    if t_idxs_t == -1:
                        not_found = False
                    else:
                        w_i = calc_Barycentric(triangle_nodes_coords_t[t_idxs_t], part_pos_i)
                        pt_i = t_idxs_t
                        inside = check_inside(w_i)
                        if inside:
                            not_found = False
                    idxx = idxx + 1

            previous_triangle_t.data[i] = pt_i
            wrong_weights_b.data[i] = not_found
            weights.data[i] = w_i

    return weights, wrong_weights_b, 0


def gpu_stepper(log_writer, released_t, bary_cache_t, triangle_3neigbors_t,
                log_prefix, n_substeps, triangle_nodes_coords_t, tree, buffer,
                kmax, particles, active_t, dX_t, triangles_simplices_t,
                nodes_t, previous_triangle_t, i, n_step):

    step_multiplier = 60.*30./n_substeps
    if log_writer:
        def log_writer_configured(quality, value):
            return log_writer.add_scalars(log_prefix+quality, value, i+n_step/n_substeps)
    else:
        log_writer_configured = False

    # 1. Calculate Step
    weights, wrong_weights_b, wrong_weights_idx = get_weights(
                                                    triangle_nodes_coords_t,
                                                    triangle_3neigbors_t,
                                                    previous_triangle_t,
                                                    tree, kmax, particles,
                                                    bary_cache_t, active_t,
                                                    log_writer=log_writer_configured)
    if log_writer:
        field_calc_start = perf_counter()

    dX_t = (buffer[i % buffer.shape[0]][triangles_simplices_t[previous_triangle_t]] * weights[:, :, None]).sum(axis=1)
    # 2. Check if Step got a NAN from field node
    nan_t_bool = torch.isnan(dX_t[:, 0])
    # 3. Partial in-place update deactivate the particles
    #    which would got a NAN step.
    active_t.data = ~(wrong_weights_b | nan_t_bool) & active_t
    particles.data += active_t[:, None] * (dX_t * step_multiplier + torch.randn_like(dX_t))

    if log_writer:
        field_calc_fin = perf_counter()
        time_field_calc = field_calc_fin-field_calc_start
        log_writer_configured('/Time', {'FieldCalculus': time_field_calc, },)
        # log_writer_configured('/Particles', {'Active': active_t_idx.shape[0], },)

    # del active_t_idx


def gpu_stepper_rk4(log_writer, released_t, bary_cache_t, triangle_3neigbors_t,
                    log_prefix, n_substeps, triangle_nodes_coords_t, tree, buffer,
                    kmax, particles, active_t, dX_t, triangles_simplices_t,
                    nodes_t, previous_triangle_t, i, n_step):
    '''
    Runge-Kutta Stepper
    '''

    step_multiplier = 60.*30./n_substeps
    if log_writer:
        def log_writer_configured(quality, value):
            return log_writer.add_scalars(log_prefix+quality, value, i+n_step/n_substeps)
        # log_writer = log_writer_configured
    else:
        log_writer_configured = False

    if log_writer:
        field_calc_start = perf_counter()

    last_field = buffer[i % buffer.shape[0]]
    next_field = buffer[(i+1) % buffer.shape[0]]

    # 1. Calculate Step
    weights, wrong_weights_b, wrong_weights_idx = get_weights(
                                                    triangle_nodes_coords_t,
                                                    triangle_3neigbors_t,
                                                    previous_triangle_t,
                                                    tree, kmax, particles,
                                                    bary_cache_t, active_t,
                                                    log_writer=log_writer_configured)

    field_t0 = last_field * (1.-n_step/n_substeps) + (n_step/n_substeps) * next_field
    field_t1 = last_field * (1.-(n_step+1)/n_substeps) + ((n_step+1)/n_substeps) * next_field

    k1 = (field_t0[triangles_simplices_t[previous_triangle_t]] * weights[:, :, None]).sum(axis=1)

    nan_t_bool = torch.isnan(k1[:, 0])
    active_t.data = ~(wrong_weights_b | nan_t_bool) & active_t

    k2_triangles = previous_triangle_t
    k2_pos = particles + step_multiplier/2.*k1

    weights_k2, w_b, _ = get_weights(
                                triangle_nodes_coords_t,
                                triangle_3neigbors_t,
                                k2_triangles,
                                tree, kmax, k2_pos,
                                bary_cache_t, active_t,
                                log_writer=log_writer_configured)

    bufferHalf = (field_t0+field_t1) * 0.5

    k2 = (bufferHalf[triangles_simplices_t[k2_triangles]] * weights_k2[:, :, None]).sum(axis=1)

    nan_t_bool = torch.isnan(k2[:, 0])
    active_t.data = ~(w_b | nan_t_bool) & active_t

    k3_triangles = k2_triangles
    k3_pos = particles + step_multiplier/2.*k2

    weights_3, wb_3, _ = get_weights(
                                    triangle_nodes_coords_t,
                                    triangle_3neigbors_t,
                                    k3_triangles,
                                    tree, kmax, k3_pos,
                                    bary_cache_t, active_t,
                                    log_writer=log_writer_configured)

    k3 = (bufferHalf[triangles_simplices_t[k3_triangles]] * weights_3[:, :, None]).sum(axis=1)

    nan_t_bool = torch.isnan(k3[:, 0])
    active_t.data = ~(wb_3 | nan_t_bool) & active_t

    k4_triangles = k3_triangles
    k4_pos = particles + step_multiplier * k3

    weights_4, wb_4, _ = get_weights(
                                    triangle_nodes_coords_t,
                                    triangle_3neigbors_t,
                                    k4_triangles,
                                    tree, kmax, k4_pos,
                                    bary_cache_t, active_t,
                                    log_writer=log_writer_configured)

    k4 = (field_t1[triangles_simplices_t[k4_triangles]] * weights_4[:, :, None]).sum(axis=1)

    dX_t = step_multiplier/6. * (k1 + 2 * (k2 + k3) + k4)

    nan_t_bool = torch.isnan(k4[:, 0])
    # active_t.data = ~(wb_4 | nan_t_bool) & active_t

    # active_t_idx = active_t.nonzero().view(-1)
    # valid_coords = dX_t[active_t_idx]

    # #  Finally
    # # 4. Partial update of all active particles. Add random walk for those active particles.
    # particles.data[active_t_idx] += valid_coords + torch.randn_like(valid_coords)

    active_t.data = ~(wb_4 | nan_t_bool) & active_t
    # active_t_idx = active_t.nonzero().view(-1)
    # valid_coords = dX_t[active_t_idx]
    #  Finally
    # 4. Partial update of all active particles. Add random walk for those active particles.
    particles.data += (dX_t + torch.randn_like(dX_t)) * active_t

    if log_writer:
        field_calc_fin = perf_counter()
        time_field_calc = field_calc_fin-field_calc_start
        log_writer_configured('/Time', {'FieldCalculus': time_field_calc, },)
        # log_writer_configured('/Particles', {'Active': active_t_idx.shape[0], },)
    return particles
