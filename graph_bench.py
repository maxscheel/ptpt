import numpy as np
import matplotlib.pyplot as plt
import glob

lw = 3

with plt.style.context('tableau-colorblind10'):
    files = glob.glob('benchmarks/*_rk4.txt')
    plt.figure(figsize=(14, 6),)
    for f in files:
        data = np.genfromtxt(f)
        name = ' '.join(f.split('_')[1:]).replace('.txt', '')
        plt.loglog(data[:, 0], data[:, 3], ls='dotted', label=name, lw=lw)

    data_CP1 = np.genfromtxt('benchmarks/bench.txt')
    data_CP4 = np.genfromtxt('benchmarks/bench_cpu4.txt')
    data_GPU = np.genfromtxt('benchmarks/bench_TitanXP.txt')
    data_AWS = np.genfromtxt('benchmarks/bench_v100.txt')
    plt.loglog(data_CP1[:, 0], data_CP1[:, 3], label='CPU (1 thread)', lw=lw)
    plt.loglog(data_CP4[:, 0], data_CP4[:, 3], label='CPU (4 threads)', lw=lw)
    plt.loglog(data_GPU[:, 0], data_GPU[:, 3], label='TitanXP', lw=lw)
    plt.loglog(data_AWS[:, 0], data_AWS[:, 3], label='V100', lw=lw)
    plt.grid(which='both')
    plt.title('Compute time for 25 steps with 60 substeps vs. Number of particles')
    plt.xlabel('Number of particles')
    plt.ylabel('Compute time [s]')
    plt.legend()
    plt.tight_layout()
    plt.savefig('./images/time_vs_particles.png')
    plt.show()

with plt.style.context('tableau-colorblind10'):
    files = glob.glob('benchmarks/*_rk4.txt')
    plt.figure(figsize=(14, 6))
    for f in files:
        data = np.genfromtxt(f)
        name = ' '.join(f.split('_')[1:]).replace('.txt', '')
        plt.loglog(data[:, 0], data[:, 3]/data[:, 0]/4/25/60*1e9, linestyle='dotted', label=name, lw=lw)

    data_CP1 = np.genfromtxt('benchmarks/bench.txt')
    data_CP4 = np.genfromtxt('benchmarks/bench_cpu4.txt')
    data_GPU = np.genfromtxt('benchmarks/bench_TitanXP.txt')
    data_AWS = np.genfromtxt('benchmarks/bench_v100.txt')
    plt.loglog(data_CP1[:, 0], data_CP1[:, 3]/data_CP1[:, 0]/25/60*1e9, label='CPU (1 thread)', lw=lw)
    plt.loglog(data_CP4[:, 0], data_CP4[:, 3]/data_CP4[:, 0]/25/60*1e9, label='CPU (4 threads)', lw=lw)
    plt.loglog(data_GPU[:, 0], data_GPU[:, 3]/data_GPU[:, 0]/25/60*1e9, label='TitanXP', lw=lw)
    plt.loglog(data_AWS[:, 0], data_AWS[:, 3]/data_AWS[:, 0]/25/60*1e9, label='V100', lw=lw)
    plt.grid(which='both')
    plt.title('Compute time/(Number of particles x steps x substeps) vs. Number of particles')
    plt.xlabel('Number of particles')
    plt.ylabel('Compute time [ns]')
    plt.legend()
    plt.tight_layout()
    plt.savefig('./images/time_ns_vs_particles.png')
    plt.show()
