# test_main.py

import torch
import numpy as np
from scipy.spatial import cKDTree
from scipy.spatial import Delaunay
import pickle

from main import calc_Barycentric
import time
import h5netcdf.legacyapi as netCDF4


from old import calc_Barycentric_cached, gen_bary_cache_t

# Conclusion:  cached approach for calc barycentric coordinates is about 20% faster than uncached - at the expense of more gram. At this stage not feasible

'''
# Bandwidth and index testing:
 - Titan X GRAM: GDDR5X   540GB/s      10.000.000 * 2Pos * 64Bit  * 1byte/(8bit) / 2**30 =  0.149 GB     --> transfer at least: 0.276 ms

'''


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


model_input = './demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc'
model = netCDF4.Dataset(model_input,'r')
nodes = np.array(list(zip(model['x'][:],model['y'][:])))
model = None

mean_node = np.mean(nodes, axis=0)
print(mean_node)
n_particles = 10 * 1000 #* 1000

particles_c = np.random.uniform(-50.,50.,(n_particles,2)) + np.asarray([1.6166e6,5.4264e6])
triangles = Delaunay(nodes)
subtract_mean = True
if subtract_mean:
    particles = torch.tensor(particles_c-mean_node, device=device).double()
    nodes_t = torch.tensor(nodes-mean_node, device=device).double()
    particles_f = torch.tensor(particles_c-mean_node, device=device).float()
    nodes_t_f = torch.tensor(nodes-mean_node, device=device).float()

# else:
#     particles = torch.tensor(particles_c, device=device).double()
#     nodes_t = torch.tensor(nodes, device=device).double()
#     particles_f = torch.tensor(particles_c, device=device).float()
#     nodes_t_f = torch.tensor(nodes, device=device).float()

triangles_simplices_t = torch.tensor(triangles.simplices, device=device).long() # this could be int32 but torch needs long
tri_nodes_f_t = nodes_t_f[triangles_simplices_t]
tri_nodes_d_t = nodes_t[triangles_simplices_t]

previous_triangle_t = torch.ones(n_particles, device=device).long()


print('genCache..', end='')
bary_cache = gen_bary_cache_t(triangles_simplices_t, nodes_t)
bary_cache_f = gen_bary_cache_t(triangles_simplices_t, nodes_t_f)
print('...done.')



print('loading tree')
pkl_file = open('tree.pkl', 'rb')
tree = pickle.load(pkl_file)
pkl_file.close()

#
for i in range(1,8):
    t_start = time.perf_counter()
    _, t_idxs_init = tree.query(particles_c-mean_node, k=[i,], eps=0.0)
    t_fin = time.perf_counter()
    print('idx', i, t_fin-t_start)

for i in range(1,8):
    t_start = time.perf_counter()
    _, t_idxs_init = tree.query(particles_c-mean_node, k=i, eps=0.0)
    t_fin = time.perf_counter()
    print('lst', i, t_fin-t_start)

_, t_idxs_init = tree.query(particles_c-mean_node, k=[1], eps=0.0)
previous_triangle_t *= int(t_idxs_init[0]) #previous triangles

increase = []
increase2 = []
min_error = []
max_error = []
for i in range(100):
    torch.cuda.synchronize()
    t_start = time.perf_counter()
    # weights1 = calc_Barycentric(nodes_t[triangles_simplices_t[previous_triangle_t]], particles)
    weights1 = calc_Barycentric(tri_nodes_f_t[previous_triangle_t], particles_f)
    torch.cuda.synchronize()
    t_fin = time.perf_counter()
    t_64 = t_fin-t_start


    # torch.cuda.synchronize()
    # t_start = time.perf_counter()
    # weights1 = calc_Barycentric(nodes_t[triangles_simplices_t[previous_triangle_t]].float(), particles.float()).double()
    # torch.cuda.synchronize()
    # t_fin = time.perf_counter()
    # t_64casted = t_fin-t_start


    torch.cuda.synchronize()
    t_start = time.perf_counter()
    # weights2 = calc_Barycentric(tri_nodes_f_t[previous_triangle_t], particles_f)
    weights2 = calc_Barycentric_cached(tri_nodes_f_t[previous_triangle_t], particles_f, bary_cache_f[previous_triangle_t])

    torch.cuda.synchronize()
    t_fin = time.perf_counter()

    t_32 = t_fin-t_start
    print(t_32, 'vs', t_64)
    # print('float32', t_32)
    val = t_32/t_64
    # val2 = t_64casted/t_64
    # print(val)
    increase.append(val)
    # increase2.append(val2)
    max_error.append((weights1-weights2).max().cpu().numpy())
    min_error.append((weights1-weights2).min().cpu().numpy())


import matplotlib.pyplot as plt
plt.figure()
plt.hist(increase,bins=10, label='float native')
# plt.hist(increase2,bins=10, label='float casted')
plt.legend()
plt.figure()
print(min(min_error), max(min_error))
print(min(max_error), max(max_error))
# plt.hist(max_error, bins=30)

plt.show()
