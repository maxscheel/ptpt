import numpy as np
from netCDF4 import Dataset
model_input = './demo/ScircFlow2D1.nc'

model = Dataset(model_input)
nodes = np.array(list(zip(model['x'][:],model['y'][:])))
def get_field_nodes(model, i):
  field_nodes = np.array(list(zip(model['u'][i,:],model['v'][i,:])))
  return field_nodes

field_nodes = get_field_nodes(model, 1)


import matplotlib.pyplot as plt
fig, ax = plt.subplots()
q = ax.quiver(nodes[:,0], nodes[:,1], field_nodes[:,0],field_nodes[:,1] )
plt.show()
