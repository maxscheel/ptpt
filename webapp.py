from typing import List

from fastapi import FastAPI
# from fastapi.staticfiles import StaticFiles

from starlette.responses import FileResponse
from starlette.websockets import WebSocket, WebSocketDisconnect

import torch

from time import perf_counter
from writer import async_listener2, async_listener_packages

from video import get_extent

import numpy as np

from main import run_single_location, get_field_from_GPU_buffered, init_model


async def custom_runner(particles, coordinate_offset_t, n_timesteps, n_substeps, profile,
                        loginterval, device, stepper_configured,
                        get_field_from_GPU_buffered_configured,
                        extent, websocket=None):

    # manager = mp.Manager()
    # messageQ = manager.Queue()
    # from arrayqueues.shared_arrays import ArrayQueue
    # resultQ = ArrayQueue(int(particles.shape[0]/7500)+10)
    result_arr = np.zeros([10, particles.shape[0], 2])

    start_run = perf_counter()
    for i in range(n_timesteps):
        # print(i)
        # if i > 0:
        #     get_field_from_GPU_buffered_configured(i)
        startint = perf_counter()
        if profile:
            with torch.autograd.profiler.profile(use_cuda=(device != 'cpu'), record_shapes=True) as prof:
                for n_step in range(n_substeps):
                    stepper_configured(i, n_step, n_substeps)
            print(prof.key_averages(group_by_input_shape=True).table())
            print(prof.key_averages().table(sort_by="self_cpu_time_total"))
        else:
            for n_step in range(n_substeps):
                stepper_configured(i, n_step, n_substeps)

        if loginterval > 0:
            if i % loginterval == 0:
                print(i, 'interval took', perf_counter()-startint)
                new_pos = (particles+coordinate_offset_t).float().cpu().numpy()
                start = perf_counter()
                nbins = (int(400), int(400))
                result_arr[i % result_arr.shape[0]] = new_pos
                if i % 10 == 9:
                    e = extent.getE()
                    print(e)
                    stuff = result_arr.copy()
                    await async_listener_packages(stuff, particles.shape[0], e, websocket, nbins)
                # await async_listener2(new_pos, particles.shape[0], e, websocket, nbins)
                print(i, 'write took', perf_counter()-start)

    end_run = perf_counter()
    print('sim took', end_run-start_run)

    # if type(result_arr) != bool:
    #     for pos in result_arr:
    #         resultQ.put(pos)
    # messageQ.put('kill')
    # watcher.join()


async def main(websocket, extent):
    log_writer = False
    log_prefix = ''
    buffersize = 50
    n_substeps = 6
    cached_bary = False
    profile = False
    n_particles = 100000
    n_timesteps = 1200
    loginterval = 1
    kmax = 10
    model_input = './demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc'
    fp32 = True

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # device = torch.device("cpu")
    dtype = torch.float32 if fp32 else torch.float64

    res = init_model(model_input, dtype, device, buffersize, n_particles, kmax, cached_bary, log_writer, log_prefix)

    stepper_configured, tree, model, buffer, coordinate_offset_t, particles, previous_triangle_t = res
    particles.data[:], previous_triangle_t.data[:] = run_single_location(tree, device, dtype, coordinate_offset_t,
                                                                         n_particles=n_particles)

    def get_field_from_GPU_buffered_configured(i):
        return get_field_from_GPU_buffered(model, i, buffer, n_substeps)

    get_field_from_GPU_buffered_configured(0)

    await custom_runner(particles, coordinate_offset_t, n_timesteps, n_substeps, profile, loginterval, device,
                        stepper_configured, get_field_from_GPU_buffered_configured, extent, websocket)


app = FastAPI()


@app.get("/")
async def get():
    return FileResponse('static/index.html')


class Notifier:
    def __init__(self):
        self.connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.connections.append(websocket)

    def remove(self, websocket: WebSocket):
        self.connections.remove(websocket)


class Extend:
    def __init__(self):
        self.data = get_extent()

    def getE(self):
        return self.data

    def setE(self, new_extent):
        self.data = new_extent


extend = Extend()
notifier = Notifier()

# https://medium.com/samsung-internet-dev/being-fast-and-light-using-binary-data-to-optimise-libraries-on-the-client-and-the-server-5709f06ef105


@app.get("/extent/{xmin}/{xmax}/{ymin}/{ymax}")
def push_to_connected_websockets(xmin: float, xmax: float, ymin: float, ymax: float):
    # await notifier.push()
    print('o', extend.getE())
    extend.setE([[xmin, xmax], [ymin, ymax]])
    print('n', extend.getE())
    return extend.getE()


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await notifier.connect(websocket)

    try:
        while True:
            data = await websocket.receive_text()
            if data == 'start':
                await main(websocket, extend)

    except WebSocketDisconnect:
        notifier.remove(websocket)
