#!/usr/bin/env python3.8

'''
PyTorch Particle Tracker - PTPT
Max Scheel 2020
max@max.ac.nz
'''


import click
import torch
import numpy as np
import multiprocessing as mp
import h5netcdf.legacyapi as netCDF4
from datetime import datetime
from time import perf_counter
from scipy.spatial import Delaunay

from torch.utils.tensorboard import SummaryWriter
from arrayqueues.shared_arrays import ArrayQueue

from writer import listener
from helper import gen_bary_cache_t, get_field_from_GPU_buffered, get_tree


def init_model(model_input, dtype, device, buffersize, n_particles,
               kmax=10, cached_bary=False, log_writer=False, log_prefix=''):

    model = netCDF4.Dataset(model_input, 'r', 'NETCDF4')
    nodes = np.asarray([model['x'][:], model['y'][:]]).T
    # print(model['time'][:])
    nm = nodes.mean(axis=0)
    nodes -= nm
    triangles = Delaunay(nodes)
    # from old import plotgrid
    # plotgrid(nodes, triangles, model)

    tree = get_tree(nodes, triangles, tree_file='{}_tree.pkl'.format(model_input))
    # Generatate Tensors
    nodes_t = torch.tensor(nodes, device=device, dtype=dtype)
    coordinate_offset_t = torch.tensor(nm, device=device, dtype=dtype)
    buffer = torch.zeros([buffersize, nodes_t.shape[0], 2], device=device, dtype=dtype)

    triangles_simplices_t = torch.tensor(triangles.simplices, device=device, dtype=torch.long)
    triangle_3neigbors_t = torch.tensor(triangles.neighbors, device=device, dtype=torch.long)
    triangle_nodes_coords_t = nodes_t[triangles_simplices_t]

    particles = torch.empty((n_particles, 2), device=device, dtype=dtype)
    previous_triangle_t = torch.ones(n_particles, device=device, dtype=torch.long)
    dX_t = torch.zeros(n_particles, device=device, dtype=dtype)
    active_t = torch.ones(n_particles, device=device, dtype=torch.bool)
    released_t = torch.ones(n_particles, device=device, dtype=torch.bool)

    bary_cache_t = gen_bary_cache_t(triangles_simplices_t, nodes_t) if cached_bary else False

    def stepper_configured(i, n_step, n_substeps):
        from helper import gpu_stepper
        # from helper import gpu_stepper_rk4 as gpu_stepper
        return gpu_stepper(log_writer, released_t, bary_cache_t, triangle_3neigbors_t,
                           log_prefix, n_substeps, triangle_nodes_coords_t, tree, buffer,
                           kmax, particles, active_t, dX_t,
                           triangles_simplices_t, nodes_t, previous_triangle_t,
                           i, n_step)

    return stepper_configured, tree, model, buffer, coordinate_offset_t, particles, previous_triangle_t


def runner(n_particles, coordinate_offset_t, n_timesteps, n_substeps, profile,
           loginterval,
           stepper_configured,
           get_field_from_GPU_buffered_configured,
           get_particles,
           outpath='', write_video=False, write_h5=False):
    manager = mp.Manager()
    messageQ = manager.Queue()
    resultQ = ArrayQueue(int(n_particles/7500)+1)
    result_arr = np.zeros((n_timesteps, n_particles, 2)) if n_particles < 1000 else False
    watcher = mp.Process(target=listener, args=(messageQ, resultQ, n_particles, outpath, write_video, write_h5))
    watcher.start()

    start_run = perf_counter()
    for i in range(n_timesteps):
        print(i)
        if i > 0:
            get_field_from_GPU_buffered_configured(i)
        startint = perf_counter()
        if profile:
            with torch.autograd.profiler.profile(use_cuda=True, record_shapes=True) as prof:
                for n_step in range(n_substeps):
                    stepper_configured(i, n_step, n_substeps)
            print(prof.key_averages(group_by_input_shape=True).table())
            print(prof.key_averages().table(sort_by="self_cpu_time_total"))
        else:
            for n_step in range(n_substeps):
                stepper_configured(i, n_step, n_substeps)

        if loginterval > 0:
            if i % loginterval == 0:
                print(i, 'interval took', perf_counter()-startint)
                start = perf_counter()
                new_pos = get_particles()
                # print(new_pos)
                if type(result_arr) != bool:
                    result_arr[i] = new_pos
                else:
                    resultQ.put(new_pos)
                print(i, 'write took', perf_counter()-start)

    end_run = perf_counter()
    print('{} sim took'.format(n_particles), end_run-start_run)

    if type(result_arr) != bool:
        for pos in result_arr:
            resultQ.put(pos)
    messageQ.put('kill')
    watcher.join()


def run_single_location(tree, device, dtype, coordinate_offset_t, lat_NZTM=1.6166e6, long_NZTM=5.4264e6,
                        src_extent=25, n_particles=int(1e6)):
    single_location = torch.tensor([lat_NZTM, long_NZTM], device=device, dtype=dtype)
    single_location -= coordinate_offset_t
    source_extent = 25
    particles_fraction = torch.randn((n_particles, 2), device=device, dtype=dtype)
    particles_fraction = particles_fraction * source_extent + single_location
    _, t_idxs_init = tree.query(particles_fraction[0].cpu().numpy(), k=[1], eps=0.0)
    previous_triangle_t_fraction = torch.ones(n_particles, device=device, dtype=torch.long)
    previous_triangle_t_fraction *= int(t_idxs_init[0])
    return particles_fraction, previous_triangle_t_fraction


@click.command()
@click.option('--model_input', default='', type=str, help='./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc',)
@click.option('--fp32', default=False, type=bool, help='FP32 mode')
@click.option('--cpu', default=False, type=bool, help='Force CPU')
@click.option('--threads', default=1, type=int, help='Number of threads',)
@click.option('--cudadev', default='cuda:0', type=str, help='Overwrite this CUDA device, if available')
@click.option('--buffersize', default=10, type=int, help='Number of time steps',)
@click.option('--n_particles', default=1e5, type=int, help='Field Angle',)
@click.option('--n_timesteps', default=1440, type=int, help='Number of time steps, 1440 with 30min timesteps',)
@click.option('--n_substeps', default=4, type=int, help='Number of steps between hydrodynamic model steps')
@click.option('--profile', default=False, type=bool, help='Profile',)
@click.option('--loginterval', default=50, type=int, help='Number of time steps between writeout',)
@click.option('--kmax', default=20, type=int, help='Number of time steps',)
@click.option('--leafsize', default=50, type=int, help='Number of time steps',)
@click.option('--cached_bary', default=False, type=bool, help='Precalculate part of bary coords (uses more GRAM)',)
@click.option('--log_writer', default=False, type=bool, help='Create tensorboard',)
@click.option('--write_video', default=False, type=bool, help='Generate Video')
@click.option('--write_h5', default=False, type=bool, help='Profile',)
@click.option('--outpath', default='./swmr.h5', type=str, help='output file',)
@click.option('--mgpu', default=False, type=bool, help='Multiple (2x) GPUs')
@click.option('--src', default=[1617586.358, 5431000.763], type=list, help='Multiple (2x) GPUs')
def main(model_input, fp32, cpu, threads, cudadev, buffersize, n_particles, n_timesteps, n_substeps, profile,
         loginterval, kmax, leafsize, cached_bary, log_writer, write_video, write_h5, outpath, mgpu, src):
    # @click.option('--src', default=[1618392.511,5445403.829], type=list, help='Multiple (2x) GPUs')
    #  INIT
    log_writer = SummaryWriter(log_dir='logs/') if log_writer else False
    log_prefix = datetime.now().isoformat()[:-10]
    log_prefix += '/fp32_{}'.format(int(fp32))
    log_prefix += '/P{}'.format(int(n_particles))
    log_prefix += '/CPU{}'.format(cpu)
    log_prefix += '/threads{}'.format(threads)
    log_prefix += '/Stps{}'.format(n_timesteps)
    log_prefix += '/sStps{}'.format(n_substeps)
    log_prefix += '/logInterval{}'.format(loginterval)
    log_prefix += '/kMax{}'.format(kmax)
    log_prefix += '/prfl{}'.format(profile)
    log_prefix += '/cchdBry{}'.format(cached_bary)

    dtype = torch.float32 if fp32 else torch.float64

    if cpu:
        device = torch.device("cpu")
        torch.set_num_threads(threads)
    else:
        device = torch.device(cudadev if torch.cuda.is_available() else "cpu")

    if mgpu:
        engine_ratio = 5./6
        n0 = int(n_particles * engine_ratio)
        n1 = int(n_particles * (1.-engine_ratio))
        res = init_model(model_input, dtype, torch.device("cuda:0"), buffersize, n0, kmax,
                         cached_bary, log_writer, log_prefix)

        res2 = init_model(model_input, dtype, torch.device("cuda:1"), buffersize, n1, kmax,
                          cached_bary, log_writer, log_prefix)
        stepper_configured1, tree, model, buffer,  coordinate_offset_t,  particles, previous_triangle_t = res
        stepper_configured2, tree, model, buffer2, coordinate_offset_t2, particles2, previous_triangle_t2 = res2

        particles.data[:], previous_triangle_t.data[:] = run_single_location(tree, torch.device("cuda:0"), dtype,
                                                                             coordinate_offset_t,
                                                                             n_particles=n0)
        particles2.data[:], previous_triangle_t2.data[:] = run_single_location(tree, torch.device("cuda:1"), dtype,
                                                                               coordinate_offset_t2,
                                                                               n_particles=n1)

        def get_particles():
            new_pos = torch.cat(((particles + coordinate_offset_t).float().cpu(),
                                 (particles2+coordinate_offset_t2).float().cpu())).numpy()
            return new_pos

        def get_field_from_GPU_buffered_configured(i):
            get_field_from_GPU_buffered(model, i, buffer)
            get_field_from_GPU_buffered(model, i, buffer2)

        def stepper_configured(i, n_step, n_substeps):
            stepper_configured1(i, n_step, n_substeps)
            stepper_configured2(i, n_step, n_substeps)

    else:
        res = init_model(model_input, dtype, device, buffersize, n_particles, kmax,
                         cached_bary, log_writer, log_prefix)
        stepper_configured, tree, model, buffer,  coordinate_offset_t,  particles, previous_triangle_t = res
        particles.data[:], previous_triangle_t.data[:] = run_single_location(tree, device, dtype,
                                                                             coordinate_offset_t,
                                                                             lat_NZTM=src[0],
                                                                             long_NZTM=src[1],
                                                                             n_particles=n_particles)

        def get_field_from_GPU_buffered_configured(i):
            get_field_from_GPU_buffered(model, i, buffer)
            # print(buffer)

        def get_particles():
            new_pos = (particles + coordinate_offset_t).float().cpu().numpy()
            # print(new_pos)
            return new_pos

    get_field_from_GPU_buffered_configured(0)

    runner(n_particles, coordinate_offset_t, n_timesteps, n_substeps, profile, loginterval,
           stepper_configured,
           get_field_from_GPU_buffered_configured,
           get_particles,
           outpath, write_video, write_h5)

    if log_writer:
        log_writer.close()


if __name__ == '__main__':
    main()
