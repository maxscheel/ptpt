prof:
	sudo nvprof --profile-from-start off -fo net.sql -- python3 main.py --debug False --profile True --n_particles 2000000 --extent 1000000. --n_nodes 100000 --angle 45 --leafsize 2  --n_timesteps 2 --loginterval 1 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --kmax 20

run:
	python3 main.py --n_particles 2000000 --extent 1000000. --n_nodes 100000 --angle 45 --leafsize 1  --n_timesteps 50 --loginterval 1 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --kmax 20

debug:
	python3 main.py --debug True --n_particles 2000000 --extent 1000000. --n_nodes 100000 --angle 45 --leafsize 1  --n_timesteps 50 --loginterval 1 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --kmax 20


tensorboard:
	tensorboard --logdir logs/ --host 0.0.0.0 &

nv_compute:
	/usr/local/NVIDIA-Nsight-Compute/nv-nsight-cu

