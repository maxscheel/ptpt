#!/bin/bash
python3 main.py --profile 0 --n_particles 100 --leafsize 1  --n_timesteps 25 --loginterval 25 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --cached_bary 0 --n_substeps 60 --kmax 10 --fp32 1 --buffersize 30 --write_video 0 --log_writer 0 |grep -e sim >> $1

python3 main.py --profile 0 --n_particles 1000 --leafsize 1  --n_timesteps 25 --loginterval 25 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --cached_bary 0 --n_substeps 60 --kmax 10 --fp32 1 --buffersize 30 --write_video 0 --log_writer 0 |grep -e sim >> $1

python3 main.py --profile 0 --n_particles 10000 --leafsize 1  --n_timesteps 25 --loginterval 25 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --cached_bary 0 --n_substeps 60 --kmax 10 --fp32 1 --buffersize 30 --write_video 0 --log_writer 0 |grep -e sim >> $1

python3 main.py --profile 0 --n_particles 100000 --leafsize 1  --n_timesteps 25 --loginterval 25 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --cached_bary 0 --n_substeps 60 --kmax 10 --fp32 1 --buffersize 30 --write_video 0 --log_writer 0 |grep -e sim >> $1

python3 main.py --profile 0 --n_particles 1000000 --leafsize 1  --n_timesteps 25 --loginterval 25 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --cached_bary 0 --n_substeps 60 --kmax 10 --fp32 1 --buffersize 30 --write_video 0 --log_writer 0 |grep -e sim >> $1

python3 main.py --profile 0 --n_particles 10000000 --leafsize 1  --n_timesteps 25 --loginterval 25 --model_input=./demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc --cached_bary 0 --n_substeps 60 --kmax 10 --fp32 1 --buffersize 30 --write_video 0 --log_writer 0 |grep -e sim >> $1
