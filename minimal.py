#!/usr/bin/env python3.8

import torch
from main import run_single_location, get_field_from_GPU_buffered, runner, init_model


def main():
    log_writer = False
    log_prefix = ''
    write_video = True
    write_h5 = False
    buffersize = 2
    n_substeps = 4
    cached_bary = False
    profile = False
    n_particles = 1000000
    n_timesteps = 100
    outpath = ''
    loginterval = 1
    kmax = 10
    model_input = './demo/TasmanGoldenBay_BacterialModel_30Days_30min.nc'
    fp32 = True

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    dtype = torch.float32 if fp32 else torch.float64

    res = init_model(model_input, dtype, device, buffersize, n_particles, kmax, cached_bary, log_writer, log_prefix)
    stepper_configured, tree, model, buffer, coordinate_offset_t, particles, previous_triangle_t = res

    particles.data[:], previous_triangle_t.data[:] = run_single_location(tree, device, dtype, coordinate_offset_t,
                                                                         n_particles=n_particles)

    def get_particles_condfigured():
        new_pos = (particles + coordinate_offset_t).float().cpu().numpy()
        return new_pos

    def get_field_from_GPU_buffered_configured(i):
        return get_field_from_GPU_buffered(model, i, buffer)

    get_field_from_GPU_buffered_configured(0)
    runner(n_particles, coordinate_offset_t, n_timesteps, n_substeps, profile, loginterval,
           stepper_configured, get_field_from_GPU_buffered_configured,
           get_particles_condfigured,
           outpath, write_video, write_h5)


if __name__ == '__main__':
    main()

    #import torch.cuda.profiler as profiler
    # https://github.com/adityaiitb/pyprof2
    # import pyprof2
    # pyprof2.init()
    #with torch.autograd.profiler.emit_nvtx():
    #     profiler.start()
    #     main()
    #     profiler.stop()
