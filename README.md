<!-- ![PTPT Logo](./images/logo.svg) -->
![PTPT Logo](./images/g8_big.png)

# PyTorch ParticleTracker

PyTorch ParticleTracker is PyTorch-backed particle tracking engine for model output files from [SCHISM](http://ccrm.vims.edu/schismweb/).

## What does PTPT do?
- Playback the prior created model in time
- Place and track moveable objects
- Move objects forced by time and space interpolated fields from the model.
- Store particle properties (position, etc), generate derived histograms `heatmaps`, movie frames..

## How?
The implementation leans heavily on the modern, highly optimised, machine learning framework [PyTorch](https://pytorch.org/). Propergating individual particles are performed as matrix (tensor) operations and benefit from CPU and GPU (if available).



## The Numbers game
### Memory Bandwidth
* Memory Bandwidth on CPUs ~ 120 - 200 GB/s
* Memory Bandwith on GPUs ~ 500 - 1400 GB/s

### Simulate in minutes instead of hours or days
Using PTPT I can produce derived simuation results in comfortable time (minutes/hours) with much higher particle count
~ 1e7 particles on a laptop 1050ti with 4GB GRAM

~ 2e7 particles eGPU Titan XP with 12GB GRAM

~ 8e7 particles on a V100  `AWS p3.2xlarge`

This results in:
* faster feedback and model development
* Ability to simulate more different scenarios
* higher precision of concentration due to higher numbers (lower weights)

# Some Background
I started this project as a friendly competition with a CPU-based (numpy & numba) Python implementation. The latter CPU version is currently being used, in production (typically ~1e6 particles), by multiple projects at the [Cawthron Institute](https://www.cawthron.org.nz). A further downscaled version (~ 1e2 particles) of this CPU-based engine is the backend for the interactive education online tool:
* [OceanPlasticSimulator v3](https://cinst.gitlab.io/particle-tracker-vue/)
* [OceanPlasticSimulator v2](http://oceanplasticsimulator.nz/wheres-our-plastic-going/?map=cs&nosplash)


# First Results
Whilst current fuctionality is still limited to basic propagation (in 2D models), benchmarks show promising reduction in computation time when utilising GPU resources.

Inofficial comparisons show that a laptop with a 4 GB discrete graphics card can outperform recent multi-socket CPU-only Xeon servers.

## Graphs
![Time Comparison](./images/time_vs_particles.png)
![Time Comparison](./images/time_ns_vs_particles.png)

## Videos
[![Single release 6 Days @ 30s timesteps](https://img.youtube.com/vi/pcS6cqphza0/0.jpg)](http://www.youtube.com/watch?v=pcS6cqphza0 "Single release 6 Days @ 30s timesteps")
[![Particles starting at the centre of each triangle](https://img.youtube.com/vi/xiE7VOANOS4/0.jpg)](https://www.youtube.com/watch?v=xiE7VOANOS4 "Particles starting at the centre of each triangle")

## MultiGPU
![TitanXP waiting for 1050ti](./images/1050ti_throttling_titanXP.png)
Multi GPU Test. The TitanXP is waiting for 1050ti

# Installation

## Install dependencies
```
sudo pip3 install -r requirements.txt
```

# Getting started


## Download demo hindcast

Download the demo [hindcast](https://drive.google.com/file/d/1oQ4-fdZIOLKIrgaH9bdwIwCZnpGK2ZZ8/view?usp=sharing).
and place it in ```demo/```

## Run
```
python3 minimal.py
```
## Playback
```
vlc out.avi
```

# Particle tracking in tessellated grids
Tracking particles in a tessellated grid with triangles of varying sizes is not a trivial task. Tracking is an iterative process in which first to position of the particle within a triangle needs to be determinded. For each node and timestep a field can be evaluated. Currently linear spacial and time interpolations are used to the determine the field at the location and time of the particle.

##  References
- [CPU particle tracker paper - Vennel, Scheel Paper TBA]()
- [Walking Location Algorithms Roman Soukal](http://graphics.zcu.cz/files/106_REP_2010_Soukal_Roman.pdf)

# Known Bottlenecks
Currently 80% of the compute time is spent PyTorch's implementation of [nonzero](https://pytorch.org/docs/stable/torch.html#torch.nonzero).
`nonzero` is used to convert a boolean mask to indicies of particles left to be searched for in neighboring triangles.

# To Do
 - [x] Barycentric Triangle Walk
 - [x] Runge-Kutta (RK4) Stepping
 - [ ] Add NVTX tags
 - [ ] Replace pytorch.nonzero with faster kernel?
 - [ ] Work on native grid instead of delaunay triangulation
 - [20%] MultiGPU Implementation (Model parallel)
 - [ ] Implement 3D tracking/ 2.5D
 - [ ] Density Histogram based on triangle_idx occurrences. Transmit only visible triangles
 - Typical Post-Process
    - [ ] Calculate the number of particles in each bin at the end of each day
    - [ ] Add sinking and random walk
    - [ ] Add height scalar field
    - [ ] Add depth as particle property
    - [ ] Calculate the number of freshly fallen particles for the day
    - Input:
        - Mass of waste in kg
        - Number of particles
            - Weight of each particle
            - Residual waste weight ```m_r = m_0 exp(-kt)``` , where k is some decay constant

    - [ ] Time of particle spent of seafloor :fire:
    - [ ] Resuspension model (up down, particle with buoyancy as a function of (salinity, temperature)
    - [ ] Input model:
    - [ ] ReleaseLocation:
        - Shape: [[x0,y0],[x1,y1],[x2,y2],[x3,y3]] # Polygon outline
        - Release_type: ['uniform im shape', 'centre of shape']
        - Pulse_interval: 60s * 5 # Release pulse_size particles every pulse_interval
        - Pulse_size: 10000 # numner of particles in pulse
        - Particle_type: ['feacal', 'food']
    - [ ] ParticleType:
        - FeacalParticle:
            - Buoyancy(salinity, temperature)
        - FoodParticle:
            - Buoyancy(salinity, temperature)


