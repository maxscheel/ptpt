import numpy as np
import matplotlib.pyplot as plt
import h5py
from mpl_toolkits.axes_grid1 import AxesGrid
from matplotlib.colorbar import Colorbar


path = './swmr.h5'

from fast_histogram import histogram2d

with h5py.File(path, 'r', libver='latest') as hf:
    fig = plt.figure(figsize=(10, 8))
    if  hf['data'].shape[0] >= 100:
        grid = AxesGrid(fig, 111, nrows_ncols=(10, int(hf['data'].shape[0]/10),),)
    elif  hf['data'].shape[0] == 20:
        grid = AxesGrid(fig, 111, nrows_ncols=(4, int(hf['data'].shape[0]/4),), )
    else:
        grid = AxesGrid(fig, 111, nrows_ncols=(1, hf['data'].shape[0],), )
    nbins = 301
    min, max = np.min(hf['data']), np.max(hf['data'])
    min, max = -10/2, 10/2
    for i, ax in enumerate(grid):
        xedges, yedges = [min,max],[min,max]
        print(hf['data'][i,:,0], hf['data'][i,:,1])
        hi = histogram2d(hf['data'][i,:,0], hf['data'][i,:,1], bins=nbins, range=[xedges, yedges])
        print(i)
        im = ax.imshow(hi, interpolation='nearest', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1] ])
        ax.set_axis_off()
        ax.set_xlim(xedges)
        ax.set_ylim(yedges)
    plt.subplots_adjust(wspace=0,hspace=0,left=0,right=1,top=1,bottom=0)
    #
    plt.show()


    # fig, axarr = plt.subplots(hf['data'].shape[0]/4, 4, figsize=(10,10))
    # axarr = axarr.ravel()
    # for i in range(len(axarr)):
    #     print(i)
    #     axarr[i].hist2d(hf['data'][i,:,0], hf['data'][i,:,1], bins=101, range=[[-1,1],[-1,1]])
    #     axarr[i].set_xlim(-1,1)
    #     axarr[i].set_ylim(-1,1)
    # plt.show()
