import numpy as np
import matplotlib.pyplot as plt
import h5py
from mpl_toolkits.axes_grid1 import AxesGrid
from matplotlib.colorbar import Colorbar


path = './swmr.h5'

from fast_histogram import histogram2d
from video import get_extent

with h5py.File(path, 'r', libver='latest') as hf:
    n_timesteps = hf['data'].shape[0]
    n_particles = hf['data'].shape[1]
    nbins = 1001

    xedges, yedges = get_extent()


    hi = None
    plt.figure()
    for i in range(n_timesteps)[::-1]:
        temp_hi = None
        if (n_timesteps<300) and (n_particles<1e4):
            plt.scatter(hf['data'][i,:,0], hf['data'][i,:,1])
        # print(hf['data'][i,:,0], hf['data'][i,:,1])
        if n_particles<1e5:
            if hi is not None:
                hi += histogram2d(hf['data'][i,:,0], hf['data'][i,:,1], bins=nbins, range=[xedges, yedges])
            else:
                hi = histogram2d(hf['data'][i,:,0], hf['data'][i,:,1], bins=nbins, range=[xedges, yedges])
        else:
            chunksize = 1000000
            for chunk in np.arange(0,n_particles,chunksize):
                print(i, chunk)
                if temp_hi is not None:
                    temp_hi += histogram2d(hf['data'][i,chunk:chunk+chunksize,0], hf['data'][i,chunk:chunk+chunksize,1], bins=nbins, range=[xedges, yedges])
                else:
                    temp_hi = histogram2d(hf['data'][i,chunk:chunk+chunksize,0], hf['data'][i,chunk:chunk+chunksize,1], bins=nbins, range=[xedges, yedges])

            if hi is not None:
                hi += temp_hi
            else:
                hi = temp_hi
            fig = plt.figure(figsize=(10, 8))
            im = plt.imshow((temp_hi.T), interpolation='nearest', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1] ], origin='upper')
            plt.show()
    # hi = np.zeros_like(hi)
    print(hi.shape)
    print(np.max(hi))
    fig = plt.figure(figsize=(10, 8))

    im = plt.imshow(np.log10(hi.T), interpolation='nearest', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1] ])
    # im = plt.imshow(hi.T, interpolation='nearest', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1] ])
    # ax.set_axis_off()
    plt.xlim(xedges[::-1])
    plt.ylim(yedges[::-1])

    plt.subplots_adjust(wspace=0,hspace=0,left=0,right=1,top=1,bottom=0)
    #
    plt.show()


    # fig, axarr = plt.subplots(hf['data'].shape[0]/4, 4, figsize=(10,10))
    # axarr = axarr.ravel()
    # for i in range(len(axarr)):
    #     print(i)
    #     axarr[i].hist2d(hf['data'][i,:,0], hf['data'][i,:,1], bins=101, range=[[-1,1],[-1,1]])
    #     axarr[i].set_xlim(-1,1)
    #     axarr[i].set_ylim(-1,1)
    # plt.show()
